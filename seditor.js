/**
 * @file
 * Simple Editor.
 *
 */

(function ($) {
  Drupal.behaviors.seditor = {
    attach: function(context) {
/*      if (!$('#wysiwyg-toggle-' + params.field).length) {
        var text = document.createTextNode(params.status ? Drupal.settings.wysiwyg.disable : Drupal.settings.wysiwyg.enable);
        var a = document.createElement('a');
        $(a).attr({ id: 'wysiwyg-toggle-' + params.field, href: 'javascript:void(0);' }).append(text);
        var div = document.createElement('div');
        $(div).addClass('wysiwyg-toggle-wrapper').append(a);
        $('#' + params.field).after(div);
      }
      $('#wysiwyg-toggle-' + params.field)
        .html(params.status ? Drupal.settings.wysiwyg.disable : Drupal.settings.wysiwyg.enable).show()
        .unbind('click.wysiwyg', Drupal.wysiwyg.toggleWysiwyg)
        .bind('click.wysiwyg', { params: params, context: context }, Drupal.wysiwyg.toggleWysiwyg);

      // Hide toggle link in case no editor is attached.
      if (params.editor == 'none') {
        $('#wysiwyg-toggle-' + params.field).hide();
      }*/
    }
  };
})(jQuery);

(function($) {

// Redefine Drupal.wysiwygAttachToggleLink to put wysiwyg toggle at the top of editor
// at the same line as body lable, otherwise same code.
Drupal.wysiwygAttachToggleLink = function(context, params) {
  if (!$('#wysiwyg-toggle-' + params.field).length) {
    var text = document.createTextNode(params.status ? Drupal.settings.wysiwyg.disable : Drupal.settings.wysiwyg.enable);
    var a = document.createElement('a');
    $(a).attr({ id: 'wysiwyg-toggle-' + params.field, href: 'javascript:void(0);' }).append(text);
    var div = document.createElement('div');
    $(div).addClass('wysiwyg-toggle-wrapper').append(a);
    $('#' + params.field).before(div);

    // Float body label to put on the same line.
    $("label[for='" + params.field + "']").attr('style', 'float: left');
  }
  $('#wysiwyg-toggle-' + params.field)
    .html(params.status ? Drupal.settings.wysiwyg.disable : Drupal.settings.wysiwyg.enable).show()
    .unbind('click.wysiwyg', Drupal.wysiwyg.toggleWysiwyg)
    .bind('click.wysiwyg', { params: params, context: context }, Drupal.wysiwyg.toggleWysiwyg);

  // Hide toggle link in case no editor is attached.
  if (params.editor == 'none') {
    $('#wysiwyg-toggle-' + params.field).hide();
  }
};

})(jQuery);
