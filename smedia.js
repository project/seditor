
(function ($) {

Drupal.behaviors.smedia = {
  attach: function () {
    // Remove upload widget to use Plupload wisget as default.
    $(".filefield-sources-list a[id$='-plupload-source']").trigger('click');
    $(".filefield-sources-list a[id$='-upload-source']").hide();

    // Place "Show" button in operatiions columns.
    $('#image-uploader td:last-child :submit').each(function() {
      $(this).after('<a href="#" class="image-show-toggle">Show</a>');
      $(this).removeClass('form-submit').closest('tr').find('.image-op-btns').append($(this));
    });

    // Attach click behavior to "Show" button.
    $('.image-show-toggle').unbind('click').click(function() {
      var toggle = $(this);
      var wrapper = toggle.closest('tr').find('.form-items-wrapper');

      if (wrapper.is(':hidden')) {
        toggle.closest('tr').find('.image-preview').removeClass('pinkynail').next('.image-widget-data').addClass('full-view');
        wrapper.fadeIn('fast');
        toggle.text(Drupal.t('Show'));
      }
      else {
        wrapper.hide();
        toggle.closest('tr').find('.image-preview').addClass('pinkynail').next('.image-widget-data').removeClass('full-view');
        toggle.text(Drupal.t('Hide'));
      }

      return false;
    });

    // Move existing divs into wrapper div to make it easy to show/hide.
    $('.form-items-wrapper').each(function () {
      $(this).siblings('div').prependTo($(this));
    });

    // Attach behavior to image insert button
    if ($('#image-uploader td :button').length) return;

    $('#image-uploader .img-insert').unbind('click').click(function() {
      var $row = $(this).closest('tr'); 
      var size = $row.find('.img-style-select input:checked').val();


      $image = $('<img />')
        .attr('title', $row.find(".image-widget-data input:[id$='title']").val().htmlEncode())
        .attr('alt', $row.find(".image-widget-data input:[id$='alt']").val().htmlEncode())
        .attr('src', $row.find('.image-preview img').attr('src').replace(/\/styles\/[\w-]+\//, '/styles/' + size + '/'));
      
      var align = $row.find('.img-align-select input:checked').val();
      switch (align) {
        case 'left':
        case 'right':
          $image.css('float', align);
          break;
        case 'center':
          $image = $image.wrap('<p style="text-align: center" />').parent();
          break;
      }
      
      if (tinyMCE.activeEditor) {
        tinyMCE.execCommand('mceInsertContent', false, $image[0].outerHTML);
      }
      else {
        $('#edit-body textarea.text-full').insertAtCaret($image[0].outerHTML);
      }

      $('#image-uploader').colorbox.close();

      return false;
    });
   
    // Pulgin for Drupal Help button
    if ($('#edit-body-und-0-format select option').length < 3) {
      $('#edit-body-und-0-format h3').text('Text Input Guide').show();
      //$('#edit-body-und-0-format').hide();
    }
    else {
      $('#edit-body-und-0-format h3').show();
      $('#edit-body-und-0-format-guidelines').not('.wrapped').addClass('wrapped').wrap('<div id="format-guidelines-wrapper" style="display: none" />');
    }

    tinymce.create('tinymce.plugins.drupalHelpPlugin', {
      createControl: function(n, cm) {
        switch (n) {
          case 'drupalhelp':
            var c = cm.createMenuButton('drupalhelp', {
              //title : 'Input Guide',
              icons : true,
              onclick: function () {
                $('#edit-body-und-0-value_drupalhelp').colorbox({
                  fixed: true,
                  transition: 'none',
                  opacity: 0.5,
                  width: 500,
                  height: '50%',
                  inline: true,
                  href: '#edit-body-und-0-format-guidelines'
                });
              }
            });
            return c;
        }
        return null;
      }
    });

    // Register plugin with a short name
    tinymce.PluginManager.add('drupalhelp', tinymce.plugins.drupalHelpPlugin);
  }
};

// Insert content into textarea
// From http://stackoverflow.com/questions/946534
$.fn.extend({
  insertAtCaret: function(myValue){
    return this.each(function(i) {
      if (document.selection) {
        //For browsers like Internet Explorer
        this.focus();
        var sel = document.selection.createRange();
        sel.text = myValue;
        this.focus();
      }
      else if (this.selectionStart || this.selectionStart == '0') {
        //For browsers like Firefox and Webkit based
        var startPos = this.selectionStart;
        var endPos = this.selectionEnd;
        var scrollTop = this.scrollTop;
        this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
        this.focus();
        this.selectionStart = startPos + myValue.length;
        this.selectionEnd = startPos + myValue.length;
        this.scrollTop = scrollTop;
      } else {
        this.value += myValue;
        this.focus();
      }
    })
  }
});

})(jQuery);

if (!String.prototype.htmlEncode) {
  String.prototype.htmlEncode = function() {
    if (this.length > 0) {
      return jQuery('<div />').text(this.toString()).html();
    } else {
      return '';
    }
  }
}
