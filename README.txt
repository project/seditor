-------------
Simple Editor
-------------


Description
----------- 

Simple Editor aims to provide easier authoring experience inspired by
Wordpress. It is essentially a wrapper to collection of some contrib modules as
dependecies and provide helper features and user interface. This currently works
with TinyMCE wysiwyg editor.


Features
--------

* Simple Editor
  - Enhanced look of toolbar icons.
  - Overrides TinyMCE init configuration.
  - Lets you exclude textareas on which you don't want wysiwyg editor.
  - Clean up edit mode user interface by hiding sections as a popup (Colorbox).
* Simple Media
  - Lets you upload multiple files by drag and drop (Plupload), import external
    images, and re-use uploaded images by other contents in the site (FileField
    Sources) as popup window.
  - Hides text input guide and shows as popup help.
* Simple oEmbed
  - Lets author embed rich media such as videos into the content by placing just
    the URL on its own line using oEmbed technology.


Requirements (Dependencies)
----------------------------

Required contributed modules and associated 3rd party libraries

* colorbox and its library
  - Library at http://www.jacklmoore.com/colorbox
* filefield_sources (7.x-1.4+17-dev or later)
* filefield_sources_plupload
* plupload and its library
  - Library at http://www.plupload.com/
* wysiwyg and TinyMCE library
  - Library at http://www.tinymce.com/download/download.php
* wysiwyg_filter


Note on version of filefield_sources
------------------------------------

If you are using feaure to add images by URL or reference existing, there is a
known issue (as of August 2012) described in
http://drupal.org/node/1329056,. The patch is applied to the dev version, but
not yet to full release version. You can find the dev version at
http://drupal.org/node/436148/release or directly download from
http://ftp.drupal.org/files/projects/filefield_sources-7.x-1.x-dev.tar.gz


Installation
------------

1) Download and place the required modules in your modules folder (usually
   sites/all/modules or sites/all/modules/contrib).

2) Downoad and place the required libraries in your libraries folder (usually
   sites/all/libraries; create libraries directory if it doesn't exist).

3) Enable the modules. You may not need all the modules and features, but you
   can decide what to remove later. It is intended to install all the modules as
   a package for best authoring experience. Eanble three modules of Simple
   Editor -- Simple Editor, Simple Media and Simple oEmbed -- plus "FileField
   Sources Plupload" module. Other required modules will be automatically
   enabled as well by confirming when you click [Save configuration] button.

4) Configure wysiwyg profile
   - Menu > Configuration > Content authoring > Wysiwyg profiles
   - Choose editor (TinyMCE) for Filtered HTML and click [Save]
   - (Optional) Click [Edit] for Filtered HTML and you can review TinyMCE
     settings, but you don't need to change anything for now. Many settings will
     be overriden in the following steps.

5) Configure Text format
   - Menu > Configuration > Content authoring > Text formats
   - Click [Configure] for Filtered HTML.
   - Under "Enabled filters" section:
   - Check "Simple oEmbed filter" and "WYSIWYG Filter".
   - Uncheck "Limit allowed HTML tags" and "Convert line breaks into HTML".
   - Under "Filter processing order" section, Place these filters in this order
     - WYSIWYG Filter
     - Simple oEmbed filter
     - Convert URLs into links (if enabled)
     Note: If you have other filters, it would be safe to place them after, but you
     may need to experiment. Note that the rows can have same order weight while
     they "appear" to be in order. Double check by clicking "Show row weights"
     and move rows up and down to assign unique row weights. The order is very
     important for Simple oEmbed to work.
   - Under "Filter settings" section:
   - Click WYSIWYG Filter and replace the content in "HTML elements and
     attributes" with the following or modify it as you like (remove indents),

     a[href|target<_blank|title|style|name],
     div[style|align<center?justify?left?right],
     p[style|align<center?justify?left?right],
     br,span[style],em,strong,cite,code,blockquote,ul,ol,li,dl,dt,dd,table,tr,td,thead,tbody,h2,h3,h4,pre,hr,sup,sub,
     img[class|style|src|border|width|height|alt|title]

     Many tags in the suggestion is needed for TinyMCE to work properly and may
     not diplay content properly without some of them.
   - Check the checkboxes for the styles. This is suggestion:
     - Font properties: font-size font-style font-weight
     - Text properties: text-align text-decoration
     - Box properties: check all
     - Dimension properties: height width
     - Layout properties: float
     Add or remove properties as you like.
   - Check Disabled for Policy or your like.
   - Click "Simple oEmbed filter" and set the maximum width of embedded media so
     media will not be bigger than your site's content width. Default is set to
     500 pixels.
   - Click [Save configuration] button.

6) Configure Simple Editor (Optional)
   - Menu > Configuration > Simple Editor
   - Simple Editor should work with the default values, but you can revisit
     later for fine tuned settings.
   - On this admin settings, you can do:
     - Override TinyMCE settings. Here, you can add/remove/rearrange toolbar
       icons. Change the settings only if you know what you are doing.
     - Exclude textareas which you don't want TinyMCE to apply.
     - Hide "Text format" dropdown.

7) Configure image and file field for a content type (Article type in this
   example)
   - Menu > Structure > Content types > Article > Manage fields
   - Add Image and File fields if they are not there yet.
   - For the Image field, click [edit]
     - Set "File directory". Leaving the textbox empty will result in uploaded
       images right under files directory.
     - Set Max image resolution. Uploaded images will be scaled to the
       max resolution. Automaticallly created derivatives of the uploaded image
       (aka image styles; thumbnail, medium, and large size by default) will
       usually be used in the contents. Since Max image resolution will scale
       the uploaded image, leaving max upload size (using system default) would
       be okay to allow users to initially upload large images, who don't know
       how to reduce image size.
     - Check "Enable Alt field" and "Enable Title field". These fields enrich
       information about uploaded images and will make it easy to search for
       images.
     - Click [File Sources] to expand the section.
     - Check "Multi file (Plupload)", "Remote URL textfield" and "Autocomplete
       reference textfield".
     - Set "Unlimited" for "Number of values". If you set it to a number <= 10,
       multi-file upload feature may not be able to not check the number of
       uploaded files until you submit the content.
     - Click [Save settings] button.
   - For the File field, click [edit]
     - Add to "Allowed file extensions". If you are not sure what to allow, this
       is a possible suggestion:
       txt doc xls pdf xlsx docx pptx jpg png gif jpeg ttf zip flv
     - Set "File directory" and "Maximum upload size", without which, system's
       limit will be used.
     - Check "Description field" (Optional).
     - Click [File Sources] to expand the section and check "Multi file
       (Plupload)". Other checkboxes that were used for Image field would
       have unlikely usage for the File field, but it is up to your site's
       situation.
     - Set "Unlimited" for "Number of values".
     - Check "Enable Display field" and "Files displayed by default". You may
       set them on your own depending on your site's requirement.
     - Click [Save settings] button.

8) Hide image field from content display
   - Menu > Structure > Content types > ARticle > Manage Display (It is one of
     the tabs if you followed last step).
   - Drag Image field to Hidden section and [Save]. Since you will be inserting
     images inside the content, you don't want to show separate image field
     outside content body.
   - Do the same for "Teaser" view mode.

9) Configure Colorbox
   - Menu > Configuration > Media > Colorbox
   - Click "Advanced settings".
   - Under "Deactivate Colorbox on specific pages", remove the lines,
     "node/add/*" and "node/*/edit".
   - Click [Save configuration] button.


